const cbor = require('cbor');
const zlib = require('zlib');

const fs = require('fs');

fileInQuestion = process.argv[process.argv.length - 1]

var d = new cbor.Decoder();
d.on('data', function(obj){
	  console.log(obj);
});

var s = fs.createReadStream(fileInQuestion);
s.pipe(zlib.createGunzip()).pipe(d);
