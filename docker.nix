{ pkgs ? import <nixpkgs> {} }:
let
  binanceCollector = import (builtins.fetchGit { url = "https://gitlab.com/qualia-cybernetics/binance-data-collector.git"; }) {};
in
  pkgs.dockerTools.buildImage {
    name = "binance-data-collector";
    contents = [ pkgs.cacert ];
    config = {
      Cmd = [ "${binanceCollector}/bin/dataCollector" ];
      WorkingDir = "/";
      Volumes = {
        "/data" = {};
      };
    };
}
