{ nixpkgs ? import <nixpkgs> {} }:
let
  overlay = self: super: {
    myHaskellPackages =
      super.haskell.packages.ghc884.override (old: {
        overrides = self.lib.composeExtensions (old.overrides or (_: _: {}))
          (hself: hsuper: {
            ghc = hsuper.ghc // { withPackages = hsuper.ghc.withHoogle; };
            ghcWithPackages = hself.ghc.withPackages;
          });
      });
  };

  pkgs = import <nixpkgs> { overlays = [ overlay ]; };

  drv = pkgs.haskellPackages.callCabal2nix "dataCollector" ./. {};

  drvWithTools = drv.env.overrideAttrs (
    old: {
      nativeBuildInputs = old.nativeBuildInputs ++ [
        pkgs.myHaskellPackages.ghcid
        pkgs.binutils
        # Add other development tools like ormolu here
      ];
      shellHook = ''

        '';
      }
  );
in
  if pkgs.lib.inNixShell then drvWithTools else drv
