{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances, DuplicateRecordFields      #-}
{-# LANGUAGE DeriveGeneric #-}
module Main where

import Data.Text (Text, pack, unpack)
import Data.Text.Encoding
import qualified Data.ByteString               as B
import qualified Data.ByteString.Lazy          as BL


import Text.Regex.TDFA
import Data.Aeson
import Data.Aeson.Types
import Data.Vector ((!))

import GHC.Generics
import Codec.Serialise as S

import System.Random
import Control.Monad (forever, unless, void)

import Wuss
import Network.WebSockets (ClientApp, receiveData, sendClose, sendTextData)
import Network.HTTP.Conduit

import System.IO.Streams (OutputStream, write)
import qualified System.IO.Streams as Streams
import System.IO.Streams.Zlib

data PLevel = PLevel
  { price  :: Double
  , amount :: Double
  } deriving (Show, Generic)

data BookInit = BookInit
  { bids :: [PLevel]
  , asks :: [PLevel]
  } deriving (Show, Generic)

data BookDiff = BookDiff
  { time   :: Integer
  , symbol :: String
  , bids   :: [PLevel]
  , asks   :: [PLevel]
  } deriving (Show, Generic)

data AbstactOrder = AbstractOrder
  { symbol :: String
  , price  :: Double
  , amount :: Double
  } deriving (Show, Generic)

data Order = Order
  { time   :: Integer
  , symbol :: String
  , price  :: Double
  , amount :: Double
  , isBuy  :: Bool
  } deriving (Show, Generic)

data Update = OrderUpdate Order | BookUpdate BookDiff | InitialBook BookInit  deriving (Show, Generic)

instance Serialise PLevel
instance FromJSON PLevel where
  parseJSON (Array a) = PLevel <$> (read <$> parseJSON ((! 0) a :: Value))
                               <*> (read <$> parseJSON ((! 1) a :: Value))

instance Serialise BookInit
instance FromJSON BookInit where
  parseJSON (Object o) = BookInit <$> o .: "bids"
                                  <*> o .: "asks"

instance Serialise BookDiff
instance FromJSON BookDiff where
  parseJSON (Object o) = BookDiff <$> o .: "E"
                                  <*> o .: "s"
                                  <*> o .: "b"
                                  <*> o .: "a"

instance Serialise Order
instance FromJSON Order where
  parseJSON (Object o) = Order <$> o .: "E"
                               <*> o .: "s"
                               <*> fmap (read) (o .: "p")
                               <*> fmap (read) (o .: "q")
                               <*> fmap not (o .: "m")

instance Serialise Update
instance FromJSON Update where
  parseJSON (Object o) = do
    lsid <- (o .:? "lastUpdateId" :: Parser (Maybe Integer))
    case lsid of
      Nothing  -> socketParse
      Just (_) -> InitialBook <$> parseJSON (Object o)
    where socketParse = do
            stream <- ((o .: "stream") :: Parser String)

            let isTrade = stream =~ (".+@aggTrade" :: String)
            let isDepth = stream =~ (".+@depth"    :: String)

            case (isTrade, isDepth) of
              (True,_)  -> OrderUpdate <$> ((o .: "data") :: Parser Order   )
              (_,True)  -> BookUpdate  <$> ((o .: "data") :: Parser BookDiff)

randomLogName :: IO String
randomLogName = fmap (\x -> ( ++ ".gz") $ ("./data/" ++ ) $ take 10 $ randomRs ('a','z') x) getStdGen

main :: IO ()
main = runSecureClient "stream.binance.com" 9443 "/stream" ws

-- Needs clean up
journalStream :: String -> IO (OutputStream B.ByteString)
journalStream logLocation = (rawJournalStream logLocation) >>= gzip (CompressionLevel 9) -- Take seralized data compress and write to file

rawJournalStream :: String -> IO (OutputStream B.ByteString)
rawJournalStream logLocation = Streams.makeOutputStream $ \m -> case m of
    Just (bs) -> B.appendFile logLocation bs
    Nothing -> return ()


logUpdate :: OutputStream B.ByteString -> Text -> IO ()
logUpdate streamOut x = do
  let mUpdate = ((decodeStrict . encodeUtf8) x :: Maybe Update)
  sequence_ $ mUpdate >>= (Just . putStrLn . (++) "logging: "  . take 100 . show)
  sequence_ $ mUpdate >>= (Just . ($ streamOut) . write . Just . B.concat . BL.toChunks . S.serialise)

ws :: ClientApp ()
ws connection = do
  logLocation <- randomLogName
  outStream <- journalStream logLocation

  let logIt = logUpdate outStream
  putStrLn $ (++) "Logging To: " logLocation

  initialBook <- simpleHttp "https://api.binance.com/api/v3/depth?symbol=BTCUSDT&limit=5000"
  (logIt . decodeUtf8 . B.concat . BL.toChunks) initialBook

  sendTextData connection $ pack "{\"method\": \"SUBSCRIBE\",\"params\": [\"btcusdt@aggTrade\",\"btcusdt@depth\"],\"id\": 1}"
  (void . forever) $ (receiveData connection :: IO Text) >>= logIt
